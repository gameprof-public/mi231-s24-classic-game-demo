using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour {
    public int height = 1;

    public virtual void TakeDamage( float damageAmount = 1f ) {
        Debug.Log( $"Enemy took damage to default base TakeDamage function. {this.gameObject.name}" );
    }
}
