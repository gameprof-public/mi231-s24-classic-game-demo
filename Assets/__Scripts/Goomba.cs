using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof(Rigidbody2D) )]
public class Goomba : Enemy {
    private Rigidbody2D r2d;

    public float     speed                 = -10;
    public Vector2   raycastWidthAndHeight = new Vector2( 0.6f, 0.1f );
    public LayerMask layersToCollideWith;

    private Collider2D thisCollider;

    void Start() {
        r2d = GetComponent<Rigidbody2D>();
        r2d.velocity = new Vector2( speed, 0 );
        thisCollider = GetComponent<Collider2D>();
    }

    void FixedUpdate() {
        // Calculate some values for the Raycast
        float direction = (speed < 0) ? -1 : 1;
        Vector3 rayOrigin = transform.position + new Vector3( 0, raycastWidthAndHeight.y, 0 );
        Vector3 rayDestination = rayOrigin + new Vector3( raycastWidthAndHeight.x * direction, 0, 0 );
        // Raycast out to see if we are about to run into a wall
        RaycastHit2D[] hits = Physics2D.RaycastAll( rayOrigin, Vector2.right * direction, 
            raycastWidthAndHeight.x, layersToCollideWith );
        bool hitSomethingElse = false;
        RaycastHit2D hit = new RaycastHit2D();
        foreach ( RaycastHit2D tempHit in hits ) {
            if ( tempHit.collider != thisCollider ) {
                hit = tempHit;
                hitSomethingElse = true;
            }
        }
        
        
        Color rayColor = Color.white;
        if ( hitSomethingElse ) {
            speed = -speed;
            rayColor = Color.red;
        }
        
        // Show this raycast using Debug.DrawLine()
        Debug.DrawLine(rayOrigin, rayDestination, rayColor, Time.fixedDeltaTime);
        
        // Update the velocity
        Vector2 vel = r2d.velocity;
        vel.x = speed;
        r2d.velocity = vel;
    }

    public override void TakeDamage( float damageAmount = 1 ) {
        Destroy( this.gameObject );
    }
}
