using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages how much damage Mario takes from Enemies.
/// This is based on their relative positions.
/// </summary>
[RequireComponent( typeof(Rigidbody2D) )]
public class PlayerDamage : MonoBehaviour {
    private Rigidbody2D r2d;

    private void OnCollisionEnter2D( Collision2D coll ) {
        if ( r2d == null ) r2d = GetComponent<Rigidbody2D>();

        Enemy enemy = coll.gameObject.GetComponent<Enemy>();
        if ( enemy == null ) return;

        // We DID hit an Enemy
        float offset = transform.position.y - enemy.transform.position.y;
        if ( offset > enemy.height - 0.5f ) {
            // Mario damaged the enemy
            Debug.LogWarning( "Gotcha!" );
            enemy.TakeDamage();
        } else {
            // Mario takes damage
            Debug.LogError("OUCH!");
        }
    }
}
